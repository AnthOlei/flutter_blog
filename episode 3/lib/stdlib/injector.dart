import 'package:flutter_blog/stdlib/config/configs.dart';
import 'package:get_it/get_it.dart';

import 'models/user.dart';

GetIt locator = GetIt.I;

void setupLocator({bool production = false}){
  if (production){
   locator.registerLazySingleton<Config>(()=> ProdConfig());
  } else {
    locator.registerLazySingleton<Config>(()=> DevConfig());
  }
  locator.registerSingleton<User>(User.empty());
}