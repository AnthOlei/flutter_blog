import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'config/config.dart';
import 'errors/failure.dart';
import 'injector.dart';
import 'models/user.dart';

Dio _dio = Dio();
final Config _config = locator<Config>();

Future<Response> makeKeylessRequest(String endpoint, {@required Map<String, dynamic> params}) async {
  final String url = _createURL(endpoint);

  final Response response = await _dio.post(url,
  data: params,
  options:Options(contentType: 'application/x-www-form-urlencoded')
  );

  return response;
}



Future<Response> makeRequest(String endpoint, {Map<String, dynamic> params}) async {

}

Future<Failure> basicDioErrorHandler (DioError e, Map<int, String> extraErrors) async {
  if (e.error is SocketException){
    return Failure(message: "Couldn't connect to our servers. Please try again soon.");
  } else if (e.response == null){
    if (!await _isConnected()){
      //no internet connection
      return Failure(message: "Couldn't connect to internet.");
    } else {
      return Failure(message: "Couldn't make connection.");
    }
  }
  Failure f = Failure(message: "Unknown Error", resolved: false);
  extraErrors.forEach((code, error){
    if (code == e.response.statusCode){
      f = Failure(message: "error", resolved: true);
    }
  });

  return f;
}

String _createURL(String endpoint) {
  String url = endpoint;
  if (url.startsWith("/")) url = url.substring(1);
  if (!url.endsWith("/")) url = "$url/";

  return "http://${_config.restBaseURL}$url";
}

Future<bool> _isConnected() async {
  final DataConnectionChecker connectionChecker = DataConnectionChecker();
  final connectivityResult = await connectionChecker.hasConnection;
  if (connectivityResult) {
    return true;
  }
  return false;
}
