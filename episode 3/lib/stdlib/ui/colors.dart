import 'dart:ui';

import 'package:flutter/widgets.dart';

enum BlogColors {
  primary,
  secondary,
  text,
  header,
}

extension ExtendedBlogColors on BlogColors {
  static const colors = {
    BlogColors.primary: Color(0xffFFAFBD),
    BlogColors.secondary: Color(0xffffc3a0),
    BlogColors.text: Color(0xFF16161d),
    BlogColors.header: Color(0xff661624),
  };


  static const gradients = {
    BlogColors.primary: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        stops: [0.1, 1.0],
        colors: [Color(0xffFFAFBD), Color(0xffffc3a0)]),
  };

  Color get color => colors[this];
  Gradient get gradient => gradients[this];
}
