import 'package:flutter/material.dart';
import 'package:flutter_blog/features/draft/presentation/draft_sheet.dart';

import 'colors.dart';

class MainScaffold extends StatefulWidget {
  final Widget body;

  const MainScaffold({Key key, this.body}) : super(key: key);

  @override
  _MainScaffoldState createState() => _MainScaffoldState();
}

class _MainScaffoldState extends State<MainScaffold> {
  Color supportColors = Color(0xffF5F5F5);
  PersistentBottomSheetController bsController;
  bool bsOpen = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: supportColors,
          leading: Icon(
            Icons.person,
            color: Colors.grey,
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Builder(
                  builder: (context) => GestureDetector(
                      onTap: () => _toggleBs(context),
                      child: Icon(
                        Icons.create,
                        color: bsOpen ? BlogColors.primary.color : Colors.grey,
                      ))),
            ),
          ],
          title: TextField(
            maxLines: 1,
            decoration: InputDecoration(prefixIcon: Icon(Icons.search)),
          ),
        ),
        body: GestureDetector(
          onTapDown: (_){
            _closeBottomSheetIfNecessary();
          },
            child: widget.body),
        backgroundColor: const Color(0xffF8F8F8),
      ),
    );
  }

  Widget _buildBottomSheet(BuildContext context) {
    return DraftSheet();
  }

  void _closeBottomSheetIfNecessary() {
    if (bsOpen) {
      bsController.close();
      setState(() {
        bsOpen = false;     
      });
    }
  }

  void _toggleBs(BuildContext context) {
    if (bsOpen) {
      bsController.close();
      setState(() {
      bsOpen = false;
      bsController = null;      
      });
    } else {
      setState(() {
        bsController =
            showBottomSheet(context: context, builder: _buildBottomSheet);
        bsOpen = true;
      });
    }
  }
}
