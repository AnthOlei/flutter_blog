import 'package:flutter/material.dart';
import 'package:flutter_blog/features/home/presentation/post.dart';
import 'package:flutter_blog/stdlib/ui/colors.dart';
import 'package:flutter_blog/stdlib/ui/main_scaffold.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainScaffold(body: Container(
        child: Scrollbar(
          child: ListView(
            children: _mockPosts(),
          ),
        ),
      ),);
  }

  List<Widget> _mockPosts() {
    List<Widget> mockPosts = List<Widget>();
    for (int i = 0; i < 10; i++) {
      mockPosts.add(Post(title: "Dart is awesome! You can make great apps.", body: "idfj dsijf iosdjf dsjfi jsdif jsdoif joisf ojvkdosidk fiosdkf oisdkf oiskdif sdik oisdf dkf oisdk fosd ifksoidkf oisdkf oiksdfio jsdoif sdojfosdijf oidsjf iosdjf oisdjfo idsjof jdoij fosidjfosdj iofjsdo jfsijd ifjsd iofjoidsjf oijsdof sdi fjsod dsifj sdoij foisdjf oisdf osd j", date: "Today"));
    }
    return mockPosts;
  }
}
