import 'package:flutter/material.dart';

class Post extends StatelessWidget {
  String title;
  String body;
  String date;

  Post({this.title, this.body, this.date});


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Text(title, textAlign: TextAlign.left, style: Theme.of(context).textTheme.headline5,),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(body, textAlign: TextAlign.left, maxLines: 3, overflow: TextOverflow.ellipsis,),
            ),
            Text(date, textAlign: TextAlign.left, style: Theme.of(context).textTheme.caption,),
          ],),
        ),
      ),
    );
  }
}