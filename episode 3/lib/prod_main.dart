import 'package:flutter/cupertino.dart';
import 'package:flutter_blog/stdlib/injector.dart';

import 'main.dart';

void main(){
  setupLocator(production: true);
  runApp(MyApp());
}