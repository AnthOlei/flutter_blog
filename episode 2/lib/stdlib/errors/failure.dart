import 'package:flutter/cupertino.dart';

class Failure{
  String message;
  bool resolved;

  Failure({@required this.message, this.resolved = true});
}

// throw error; index out of bounds, null pointer
// Failure() <--- messages meant for user