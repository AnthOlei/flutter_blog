import 'config.dart';

class DevConfig extends Config{
  @override
  String get restBaseURL => "localhost:5000/";
}