import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class User{
  String apiKey;
  String email;
  int id;

  User();

  Future<String> getRefreshToken(){
    const storage = FlutterSecureStorage();
    final value = storage.read(key: "refresh_token");
    return value;
  }

  void setRefreshToken(String token){
    const storage = FlutterSecureStorage();
    storage.write(key: "refresh_token", value: token);
  }

  void signOut(){
    apiKey = null;
    const storage = FlutterSecureStorage();
    storage.delete(key: "refresh_token");
  }
}