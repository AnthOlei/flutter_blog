import 'package:flutter/material.dart';
import 'package:flutter_blog/stdlib/models/post.dart';

class PostDisplay extends StatelessWidget {
  Post post;

  PostDisplay({this.post});


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
            Text(post.title, textAlign: TextAlign.left, style: Theme.of(context).textTheme.headline5,),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(post.body, textAlign: TextAlign.left, maxLines: 3, overflow: TextOverflow.ellipsis,),
            ),
            Text(post.date, textAlign: TextAlign.left, style: Theme.of(context).textTheme.caption,),
          ],),
        ),
      ),
    );
  }
}