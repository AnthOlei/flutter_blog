import 'package:flutter/material.dart';
import 'package:flutter_blog/features/home/presentation/display_post.dart';
import 'package:flutter_blog/stdlib/models/post.dart';
import 'package:flutter_blog/stdlib/ui/main_scaffold.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      body: Scrollbar(child: ListView(children: _mockPosts())),
    );
  }

  List<Widget> _mockPosts() {
    List<Widget> mockPosts = List<Widget>();
    for (int i = 0; i < 10; i++) {
      mockPosts.add(PostDisplay(
          post: Post(
              title: "Dart is awesome! You can make great apps.",
              body:
                  "I love darja idj iasjdi ajsidj aisjd iasi djaij diasj ijd ajs jdaij it.  dart is realy great. asidjaodjasodjoiasjdoiajsjosadjasioj doiasjd oajsodi jasoidj oaisjdo iasjodi jasoi jdoiasjd ojasodj aosi j",
              date: "Today")));
    }
    return mockPosts;
  }
}
