part of 'writepost_bloc.dart';

@immutable
abstract class WritepostState {}

class WritepostInitial extends WritepostState {}

class WritepostLoading extends WritepostState {}

class WritePostFailure extends WritepostState {
  final Failure failure;

  WritePostFailure(this.failure);
}

class WritePostCompleted extends WritepostState{}