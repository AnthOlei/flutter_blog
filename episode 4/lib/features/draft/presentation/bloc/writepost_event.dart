part of 'writepost_bloc.dart';

@immutable
abstract class WritepostEvent {}

class SendPostEvent extends WritepostEvent {
  final Post post;

  SendPostEvent(this.post);
}

class ResetFormEvent extends WritepostEvent{}