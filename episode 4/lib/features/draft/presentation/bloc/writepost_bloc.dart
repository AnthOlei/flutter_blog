import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:flutter_blog/stdlib/errors/failure.dart';
import 'package:flutter_blog/stdlib/httpClient.dart';
import 'package:flutter_blog/stdlib/models/post.dart';
import 'package:meta/meta.dart';

part 'writepost_event.dart';
part 'writepost_state.dart';

class WritepostBloc extends Bloc<WritepostEvent, WritepostState> {
  @override
  WritepostState get initialState => WritepostInitial();

  @override
  Stream<WritepostState> mapEventToState(WritepostEvent event) async* {
    if (event is SendPostEvent){
      yield WritepostLoading();
      try {
        await makeRequest("/post/", params: {
          "title": event.post.title,
          "author": event.post.author,
          "body": event.post.body,
          "catagory": "flutter",
        });
        yield WritePostCompleted();
      } on DioError catch (e){
        yield WritePostFailure(await basicDioErrorHandler(e, {}));
      }
    } else if (event is ResetFormEvent){
      yield WritepostInitial();
    }
  }
}
