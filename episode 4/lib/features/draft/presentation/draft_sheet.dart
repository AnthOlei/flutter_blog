import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blog/stdlib/errors/failure.dart';
import 'package:flutter_blog/stdlib/injector.dart';
import 'package:flutter_blog/stdlib/models/post.dart';
import 'package:flutter_blog/stdlib/models/user.dart';

import 'bloc/writepost_bloc.dart';

class DraftSheet extends StatelessWidget {
  final WritepostBloc _writePostBloc = WritepostBloc();
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _bodyController = TextEditingController();  

  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 40.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(20)),
            color: Colors.white,
            boxShadow: [
            BoxShadow(blurRadius: 2, spreadRadius: 2, color: Colors.grey)
          ]),
          width: double.infinity,
          child:  Padding(
            padding:  const EdgeInsets.all(8.0),
            child: BlocBuilder<WritepostBloc, WritepostState>(
              bloc: _writePostBloc,
              builder: (context, state){
                if (state is WritepostInitial){
                  return _buildForm();
                } else if (state is WritePostFailure){
                  return _buildFailure(state.failure);
                } else if (state is WritePostCompleted){
                  return _buildCompleted();
                } else if (state is WritepostLoading){
                  return const CupertinoActivityIndicator(animating: true,);
                } else {
                  throw Exception("unhandled state");
                }

              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFailure(Failure f){
    return Text(f.message);
  }

  Widget _buildForm(){
        return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
      TextField(controller: _titleController,),
      TextField(controller: _bodyController,),
      FlatButton(child: Text("submit post"), onPressed: _submitPost,),
    ],);
  }

  void _resetBloc(){
    _writePostBloc.add(ResetFormEvent());
    _titleController.clear();
    _bodyController.clear();
  }

  Widget _buildCompleted(){
        return Column(
          mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text("completed"),
        FlatButton(child: Text("submit another post"), onPressed: _resetBloc,),
      ],
    );
  }

  void _submitPost(){
    _writePostBloc.add(SendPostEvent(Post(
      author: locator<User>().email,
      body: _bodyController.text,
      title: _titleController.text,
      date: DateTime.now().toString(),
    )));
  }
}