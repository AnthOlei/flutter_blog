import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blog/features/login/domain/login.dart';
import 'package:flutter_blog/stdlib/errors/failure.dart';
import 'package:flutter_blog/stdlib/httpClient.dart';
import 'package:flutter_blog/stdlib/ui/colors.dart';
import 'package:dio/dio.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String message = "";
  Color messageColor = Colors.red;
  bool _loading = false;

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          gradient: BlogColors.primary.gradient,
        ),
        child: Center(
            child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 19.0),
          child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      color: Colors.black38,
                      offset: const Offset(3, 4),
                      spreadRadius: 3,
                      blurRadius: 3)
                ],
                borderRadius: const BorderRadius.all(Radius.circular(15)),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "Log In",
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    Text(message,
                        style: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .copyWith(color: messageColor)),
                    _buildField("email", _emailController, context,
                        secure: false, icon: Icons.email),
                    _buildField("password", _passwordController, context,
                        secure: true, icon: Icons.lock),
                    MaterialButton(
                      onPressed: _loginFunction,
                      color: Theme.of(context).primaryColor,
                      child: _determineInButtonWidget(),
                    ),
                  ],
                ),
              )),
        )),
      ),
    );
  }

  Future<void> _loginFunction() async {
    _isLoading(true);
    try {
      Response response = await makeKeylessRequest("/user/login", params: {
        "email": _emailController.text,
        "password": _passwordController.text,
      });
      login(response);
    } on DioError catch (e){
      String errorMessage;
      Failure f = await basicDioErrorHandler(e, {
        404: "email not found.",
        401: "Wrong password.",
      });
      f.message = errorMessage;
      setState(() {
        message = errorMessage;
      });
    }

    _isLoading(false);
  }

  Widget _determineInButtonWidget() {
    if (_loading){
      return const CupertinoActivityIndicator(animating: true,);
    } else {
      return Text("Log In".toUpperCase(), style: TextStyle(color: Colors.white));
    }
  }

  Widget _buildField(
      String text, TextEditingController controller, BuildContext context,
      {bool secure = false, IconData icon}) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10),
        child: Padding(
            padding: const EdgeInsets.all(3.0),
            child: TextField(
                obscureText: secure,
                controller: controller,
                cursorColor: Theme.of(context).cursorColor,
                decoration: InputDecoration(
                  prefixIcon: Icon(icon),
                  hintText: text,
                ))));
  }

  void _isLoading(bool loading) {
    if (loading) {
      setState(() {
        _loading = true;
        message = "";
      });
    } else {
      setState(() {
        _loading = false;
      });
    }
  }
}
