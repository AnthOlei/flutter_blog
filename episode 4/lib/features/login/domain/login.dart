import 'package:dio/dio.dart';
import 'package:flutter_blog/stdlib/injector.dart';
import 'package:flutter_blog/stdlib/models/user.dart';
import 'package:flutter_blog/stdlib/router/router.gr.dart';

void login(Response loginResponse){
  final Map<String, dynamic> loginResponseAsJson = loginResponse.data as Map<String, dynamic>;
  final User user = User.fromJson(loginResponseAsJson);
  locator<User>().setUser(user);
  User.setRefreshToken(loginResponseAsJson["refresh_token"] as String);
  Router.navigatorKey.currentState.pushReplacementNamed("/home");
}