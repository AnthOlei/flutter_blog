import 'package:flutter/material.dart';
import 'package:flutter_blog/stdlib/injector.dart';
import 'package:flutter_blog/stdlib/router/router.gr.dart';
import 'package:flutter_blog/stdlib/ui/colors.dart';

void main(){
  setupLocator(production: false);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Blog',
      theme: ThemeData(
        primaryColor: BlogColors.primary.color,
        accentColor: BlogColors.secondary.color,
        brightness: Brightness.light,
        cursorColor: BlogColors.primary.color,
        textSelectionColor: BlogColors.primary.color,
        fontFamily: "Avenir Next",
        textTheme: TextTheme(
          headline5: TextStyle(
            fontSize: 18,
            fontWeight: FontWeight.bold,
          ),
            headline1: TextStyle(
                fontSize: 36,
                color: BlogColors.header.color,
                fontWeight: FontWeight.bold),
            bodyText2: TextStyle(color: BlogColors.text.color),
            headline6: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)
            ),
      ),
      onGenerateRoute: (settings) => Router.onGenerateRoute(settings),
      navigatorKey: Router.navigatorKey,
      initialRoute: "/login",
    );
  }
}
