import 'package:flutter/material.dart';
import 'package:flutter_blog/features/draft/presentation/draft_sheet.dart';
import 'package:flutter_blog/stdlib/ui/colors.dart';

class MainScaffold extends StatefulWidget {
  final Widget body;

  const MainScaffold({Key key, this.body}) : super(key: key);

  @override
  _MainScaffoldState createState() => _MainScaffoldState();
}

class _MainScaffoldState extends State<MainScaffold> {
  PersistentBottomSheetController bsController;
  bool bsIsOpen = false;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
          child: Scaffold(
        appBar: AppBar(
          backgroundColor: const Color(0xffF5F5F5),
          title: TextField(
            maxLines: 1,
            decoration: InputDecoration(prefixIcon: Icon(Icons.search)),
          ),
          leading: Icon(Icons.person, color: Colors.grey),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Builder(
                builder: (context) => GestureDetector(
                    onTap: () => _toggleBs(context),
                    child: Icon(
                      Icons.create,
                      color: bsIsOpen ? BlogColors.primary.color : Colors.grey,
                    )),
              ),
            ),
          ],
        ),
        body: GestureDetector(
          onTapDown: (_){
            _closeBottomSheetIfNecessary();
          },
          child: widget.body
          ),
      ),
    );
  }

  Widget _buildBottomSheet(BuildContext context) {
    return DraftSheet();
  }

    void _closeBottomSheetIfNecessary() {
    if (bsIsOpen) {
      bsController.close();
      setState(() {
        bsIsOpen = false;     
      });
    }
  }


  void _toggleBs(BuildContext context){
    if (bsIsOpen){
      bsController.close();
      setState(() {
        bsIsOpen = false;
        bsController = null;
      });
    } else {
      setState(() {
        bsController = showBottomSheet(context: context, builder: _buildBottomSheet);
      bsIsOpen = true;
      });
    }
  }
}




///  root
///    |
///   child <-- Navigator <-- Navigator.of(context)
///    |
///   child <-- Navigator.of(context)
///     
/// Navigator.of(context)
///
///
