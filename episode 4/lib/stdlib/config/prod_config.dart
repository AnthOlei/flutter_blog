import 'package:flutter_blog/stdlib/config/config.dart';

class ProdConfig extends Config{
  @override
  String get restBaseURL => "website.com/REST/";

}