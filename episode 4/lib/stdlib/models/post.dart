import 'package:flutter/cupertino.dart';

class Post {
  String title;
  String body;
  String date;
  String author;

  Post(
      {@required this.title,
      @required this.body,
      @required this.date,
      @required this.author});
}
